<?php

namespace Drupal\session_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;

/**
 * Class SessionList.
 *
 * @package Drupal\session_list\Controller
 */
class SessionList extends ControllerBase {

  /**
   * Index.
   *
   * @return array
   *   A build array in the format expected by drupal_render().
   */
  public function index() {
    $client = new Client();
    $res = $client->request('GET', 'http://pacira-poc.local/api/sessions');

    if($res->getStatusCode() === 200) {
      //print_r($res->getBody());
      $sessions = $res->getBody();
      $result = json_decode($sessions);
      //print_r(json_encode($result));


    }

    return [
      '#theme' => 'session_list',
      '#attached' => [
        'drupalSettings' => [
          'sessions' => json_decode($sessions),
        ],
        'library' => [
          'session_list/sessions'
        ],
      ],
    ];
  }

}
